#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_a13x.mk

COMMON_LUNCH_CHOICES := \
    lineage_a13x-user \
    lineage_a13x-userdebug \
    lineage_a13x-eng
